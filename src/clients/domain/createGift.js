const { createGift } = require("../helper/createGift");
const { updateClient } = require("../service/updateClient");

module.exports = async (eventPayload, eventMeta) => {
  let payload = JSON.parse(eventPayload.Message);
  let gift = createGift(payload);
  await updateClient(payload?.dni, "gift", gift);
  return { status: 200, body: "Client gift updated" };
};
