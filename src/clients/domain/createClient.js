const { CreateClientValidation } = require("../schema/input/createClient");
const { createClientService } = require("../service/createClient");
const { ClientCreatedValidation } = require("../schema/event/clientCreated");
const { publishClientCreated } = require("../service/publishClientCreated");

module.exports = async (commandPayload, commandMeta) => {
  let InputValidation = new CreateClientValidation(commandPayload, commandMeta);
  await createClientService(InputValidation);
  let publishPayload = new ClientCreatedValidation(commandPayload, commandMeta);
  await publishClientCreated(publishPayload);
  return { status: 200, body: "Client created" };
};
