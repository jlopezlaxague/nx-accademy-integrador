const { createCard } = require("../helper/createCard");
const { updateClient } = require("../service/updateClient");

module.exports = async (eventPayload, eventMeta) => {
  let payload = JSON.parse(eventPayload.Message);
  let card = createCard(payload);
  await updateClient(payload?.dni, "card", card);
  return { status: 200, body: "Client card updated" };
};
