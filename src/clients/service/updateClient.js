const dynamo = require("ebased/service/storage/dynamo");
const TABLE_NAME = process.env.TABLE_NAME;

const updateClient = async (dniNumber, updateKey, updateValue) => {
  let params = {
    ExpressionAttributeNames: {
      "#C": updateKey,
    },
    ExpressionAttributeValues: {
      ":c": updateValue,
    },
    Key: {
      dni: dniNumber,
    },
    ReturnValues: "ALL_NEW",
    TableName: TABLE_NAME,
    UpdateExpression: "SET #C = :c",
  };
  const { Attributes } = await dynamo.updateItem(params);
  Object.keys(Attributes).forEach((k) => {
    if (k === "pk" || k === "sk") delete Attributes[k];
  });
  return Attributes;
};

module.exports = { updateClient };
