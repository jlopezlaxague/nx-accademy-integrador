const dynamo = require("ebased/service/storage/dynamo");
const config = require("ebased/util/config");

const TABLE_NAME = config.get("TABLE_NAME");

const createClientService = async (InputValidation) => {
  let payload = InputValidation.get();
  return dynamo.putItem({ TableName: TABLE_NAME, Item: payload });
};

module.exports = { createClientService };
