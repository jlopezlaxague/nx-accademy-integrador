const { InputValidation } = require("ebased/schema/inputValidation");

class CreateGiftValidation extends InputValidation {
  constructor(payload, meta) {
    super({
      type: "CLIENT.CREATE_GIFT",
      specversion: "v1.0.0",
      source: meta.source,
      payload: payload,
      schema: {
        dni: { type: String, required: true },
        name: { type: String, required: true },
        lastName: { type: String, required: true },
        birthDate: { type: String, required: true },
      },
    });
  }
}

module.exports = { CreateGiftValidation };
